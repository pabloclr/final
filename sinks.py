"""Methods for storing sound, or playing it"""

import config
import sounddevice
import soundfile

def play(sound):
    sounddevice.play(sound, config.samples_second)
    sounddevice.wait()


def _mean(sound):
    total = 0
    for sample in sound:
        total += abs(sample)
    return int(total / len(sound))


def draw(sound, period: float):
    samples_period = int(period * config.samples_second)
    for nsample in range(0, len(sound), samples_period):
        chunk = sound[nsample: nsample + samples_period]
        mean_sample = _mean(chunk)
        stars = mean_sample // 1000
        print('*' * stars)


'''
Requisito obligatorio: store, el cual almacena el sonido en un fichero .wav
La funcion square acepta como parametros sound (array de muestras de sonido) y path (camino del fichero donde se almacenara el sonido)
Mediante soundfile, podemos leer y escribir ficheros de sonido. Como queremos almacenarlo utilizamos soundfile.write
'''
def store(sound, path:str):

   print("Su sonido se esta guardando, espere un momento por favor")
   print('')
   soundfile.write(path,sound,config.samples_second)
   print("Se ha guardado con exito.")


