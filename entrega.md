Practica realizada por: Pablo Clarke Alvarez

Requisitos minimos:

-Metodo square, completando el modulo sources.py

-Metodo store, en el modulo sinks

-Metodos ampli, reduce y extend en el modulo processses.py

-Extension de source_sink.py

-Creacion del modulo source_process_sink.py

Requisitos opcionales:

-Metodo round, en el modulo processes.py

-Creacion de sound_process.py

-Metodo add, en el modulo processes.py
