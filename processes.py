import config
import array
import sources

'''
Requisito obligatorio:

'''
def ampli (sound,factor:float): #--amplificará el sonido multiplicando el valor de cada muestra por un número dado
    for i in range (len(sound)): #--Bucle de iteracion para cada muestra.
        if sound[i] * factor >= config.max_amp: #-- Si la multiplicacion es mayor a la maxima amplitud
            sound[i]=config.max_amp
        elif sound[i] * factor >= -config.max_amp: #--Si la multiplicacion es menor a la minima amplitud
            sound[i]=-config.max_amp
        elif sound[i] * factor < configa.max_amp and sound[i] * factor > -config.max_amp:
            sound[i]= int(sound[i]*factor)
        return sound

def reduce (sound,factor:int) : #--Reduce el numero de muestras
    suprimirsound=sound[0::factor ]
    while len(suprimirsound)!= 0:
        for i in sound :
            if i in suprimirsound:
                sound.remove(i)
                suprimirsound.remove(i)
    return sound

def extend (sound, factor:int): #--Extiende el numero de muestra
    if factor > 0: #--Factor positivo
        for i in range (len(sound)):
            i+=1
            if i%factor == 0:
                new=((sound[i]+sound[i+1])//2) #-- New: valor de la nueva muestra
                sound.insert(i,new) #--Insertamos el valor de la siguiente muestra a i
    return sound

def round (sound, samples:int):
    for i in range (len(sound)):
        sound2= array.array('h',[0]*len(sound))
        sound2.insert(i,sound[i])
        if i> samples+1:
            contador = samples
            sum = 0
            while contador !=0:
                sum = sum + sound[i-contador] + sound[i + contador]
                contador =-1
            sum = sum + sound[i]
            med=int(sum)//(int(samples*2)+1)
            sound2.insert(i, med)
    return sound2

def add (sound,duration,psound2:float):
    sound2 = sources.sin(duration,psound2)
    for i in range(len(sound)):
        while len(sound2)==i:
            new=(int(sound[i])+int(sound2[i]))
            sound[i]=new
    return sound

