import sys

import sinks
import sources
import processes
'''
Se creará un nuevo módulo que pueda funcionar como progrma principal, con la misma estructura que source_sink.py.
Este programa ejecutará primero una fuente, después un procesador, y por último un sumidero de sonido. 
Admitirá como argumentos un nombre de fuente, uno de procesador, uno de sumidero, y una duración.

'''
def parse_args():
    if len(sys.argv) != 5:
        print(f"Usage: {sys.argv[0]}<source> <process> <sink> <duration>")
        exit(0)
    source = sys.argv[1]
    process = sys.argv[2]
    sink = sys.argv[3]
    duration = float(sys.argv[4])

    return source, process, sink, duration

def main():
    source, process, sink, dur = parse_args()

    if source == 'sin':
        sound = sources.sin(duration=dur, freq=400)
    elif source == 'constant':
        sound= sources.constant(duration=dur, positive = True)
    elif source == 'square':
        sound = sources.square(duration=dur, freq=400)

    else:
        sys.exit("Unknown source")

    if process =='amp':
        processes.ampli(sound, factor = 2)
    elif process == 'reduce':
        processes.reduce(sound, factor=3)
    elif process == 'extend':
        processes.extend(sound, factor=2)
    else:
        sys.exit("unknown process")

    if sink == "play":
        sinks.play(sound)
    elif sink == "draw":
        sinks.draw(sound=sound, period=0.001)
    elif sink == "store":
        sinks.store(sound=sound,path="stored.wav")
    else:
        sys.exiy("Uknown sink")

if __name__ == '__main__':
    main()




