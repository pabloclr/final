import sys
import sinks
import sources
'''
Se extenderá este módulo para que acepte como posible fuente (source) de sonido square (además de las que ya acepta) y 
como posible sumidero (sink) de sonido store. Si se especifica square, se ejecutará el mérodo square con una frecuencia de 400 hertz, 
y si se especifica store, se ejecutará el método store con el nombre de fichero stored.wav.


'''

def parse_args():
    if len(sys.argv) != 4:
        print(f"Usage: {sys.argv[0]} <source> <sink> <duration>")

        exit(0)
    source = sys.argv[1]
    sink = sys.argv[2]
    duration = float(sys.argv[3])
    return source, sink, duration
'''
Asignacion de valores en las posiciones correspondientes
'''
def main():
    source, sink, dur = parse_args()

    if source == 'sin':
        sound = sources.sin(duration=dur, freq=400)
    elif source == 'constant':
        sound = sources.constant(duration=dur, positive=True)
    elif source == 'square':
        sound=sources.square(duration=dur, freq = 400)
    else:
        sys.exit("Unknown source")


    if sink == "play":
        sinks.play(sound)
    elif sink == "draw":
        sinks.draw(sound=sound, period=0.0001)
    elif sink == "store":
        sinks.store(sound=sound, path= "stored.wav")
    else:
        sys.exit("Unknown sink")
'''
Llamada a el modulo de sinks dependiendo de que funcion queremos probar 
'''
'''
Llamada al modulo de sources dependiendo sobre que señal queremos trabajar
'''

if __name__ == '__main__':
    main()
