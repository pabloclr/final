"""Methods for producing sound (sound sources)"""

import array
import math

import config

'''
Las funciones devuelven el sonido como un vector de muestras (cada muestra es un numero de dos bytes)

'''
def _samples(duration):
    """Compute the number of samples for a certain duration (sec)"""

    # Total number of samples (samples per second by number of seconds)
    return int(config.samples_second * duration)


def sin(duration: float, freq: float):
    """Produce a list of samples of a sinusoidal signal, of duratioon (sec)"""

    nsamples = _samples(duration)

    # Create an array of integers (signed shorts: 16 bit signed integers)
    sound = array.array('h', [0] * nsamples)

    # Compute sin samples (maximum amplitude, freq frequency)
    for nsample in range(nsamples):
        t = nsample / config.samples_second
        sound[nsample] = int(config.max_amp *
                             math.sin(2 * config.pi * freq * t))

    return sound


def constant(duration: float, positive: bool):
    """Produce a list of samples of a constant signal, of duration (sec)"""

    nsamples = _samples(duration)

    # Create an array of integers with either max or min amplitude
    if positive:
        sample = config.max_amp
    else:
        sample = - config.max_amp

    sound = array.array('h', [sample] * nsamples)
    return sound


def square(duration: float, freq: float):
    nsamples = _samples(duration)

    sound= array.array('h',[0] *int(nsamples))
#--Calculamos el numero de semiciclos para hallar el tiempo de la señal a maxima amplitud y a minima amplitud
    semiciclo_dur = 1 /(freq *2) #--Duracion del semiciclo
    for nsample in range(nsamples):
        t= nsamples/config.samples_second #--Periodo de la señal
        semiciclo_n=int(semiciclo_dur/t)
        if semiciclo_n % 2 == 0: #--Si el numero de semicilos de la señal es impar, la amplitud es minima, y si es par maxima.
            sound[nsample]= config.max_amp
        else:
            sound[nsample]= -config.max_amp
    return sound
