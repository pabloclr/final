import sys

import sinks
import processes
import sources

'''
Requisito opcional: el modulo sera parecido a source.sink. El programa ejecutara primero una fuente, luego un procesador y por ultimo un sumidero de sonido.
Como argumentos admitira: dmitirá como argumentos un nombre de fuente, sus parámetros, uno, ninguno, o varios nombres de procesador con sus parámetros, 
y finalmente un nombre de sumidero, sus parámetros, y una duración. 

python3 sound_process.py <source> <source_args> [<process> <process_args> ]* <sink> <sink_args> <duration>
Como ejemplo podremos poner: sin 400 amp 0.5 draw 0.001 3

'''
def parse_args():
    if len(sys.argv) != 8:
        print(f"Usage: {sys.argv[0]}<source> <source_args> <process> <process_args > <sink> <sink_args> <duration>")
        exit(0)
    source= sys.argv[1]
    source_args=sys.argv[2]
    process=sys.argv[3]
    process_args=sys.argv[4]
    sink=sys.argv[5]
    sink_args=sys.argv[6]
    duration= float(sys.argv[7])
    return source,source_args,process, process_args, sink, sink_args, duration

def main():
    source,source_args,process, process_args, sink, sink_args, duration= parse_args()

    if source == 'sin':
        sound = sources.sin(duration=duration, freq=float(source_args))
    elif source == 'constant':
        sound = sources.constant(duration=duration, positive=bool(source_args))
    elif source == 'square':
        sound = sources.square(duration=duration, freq=float(source_args))
    else:
        sys.exit("Unknown source")

    if process =='amp':
         processes.ampli(sound, factor = float(process_args))
    elif process == 'reduce':
        processes.reduce(sound, factor=int(process_args))
    elif process == 'extend':
         processes.extend(sound, factor=int(process_args))
    elif process == 'add':
         processes.add(sound,duration=duration,psound2=float(process_args))
    else:
        sys.exit("unknown process")

    if sink == "play":
        sink.play(sound)
    elif sink == "draw":
        sinks.draw(sound=sound, period=float(sink_args))
    elif sink == "store":
        sinks.store(sound=sound, path=str(sink_args))
    else:
        sys.exiy("Uknown sink")

if __name__ == '__main__':
    main()